<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobOffersTable extends Migration {

	public function up()
	{
		Schema::create('job_offers', function(Blueprint $table) {
			$table->increments('id');
			$table->text('reference');
			$table->text('society_name');
			$table->text('contact_name');
			$table->text('contact_email');
			$table->string('contact_phone', 15);
			$table->text('description');
			$table->tinyInteger('activated');
			$table->text('notes');
			$table->text('job_title');
			$table->enum('job_type', array('Fulltime', 'Parttime', 'Temporary', 'Freelance', 'Seasonal'));
			$table->text('location')->nullable();
			$table->enum('job_category', array('Commercial', 'Retailsales', 'Creative', 'Technology', 'Marketing&PR', 'Fashion&luxury', 'Management&HR'));
			$table->date('closing_at');
			$table->string('salary', 40)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('client_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('job_offers');
	}
}