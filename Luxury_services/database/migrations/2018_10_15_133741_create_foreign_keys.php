<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('job_offers', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('job_applications', function(Blueprint $table) {
			$table->foreign('users_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('job_applications', function(Blueprint $table) {
			$table->foreign('job_offers_id')->references('id')->on('job_offers')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('job_offers', function(Blueprint $table) {
			$table->dropForeign('job_offers_client_id_foreign');
		});
		Schema::table('job_applications', function(Blueprint $table) {
			$table->dropForeign('job_applications_users_id_foreign');
		});
		Schema::table('job_applications', function(Blueprint $table) {
			$table->dropForeign('job_applications_job_offers_id_foreign');
		});
	}
}