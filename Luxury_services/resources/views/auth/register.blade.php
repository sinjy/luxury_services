@extends('layouts.layout')

<style>
    .page-title-bg {
        background-image: url(../../assets/img/bg3.jpg);
    }
    .log-section form .checkbox label {
        font-size: 16px
    }
    .log-section form .checkbox label a {
        color: #b6a575;
        text-decoration: underline;
        text-transform: inherit;
        font-size: 16px
    }
    .log-section form .links a {
        display: block;
        text-align: center;
        color: #b6a575;
        text-decoration: underline;
        text-transform: inherit;
        font-size: 16px
    }
</style>

@section('content')
    <!-- Page Content-->
    <section class="section-padding gray-bg log-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 card card-panel">
                    <h3 class="text-extrabold">Create a new account</h3>
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form id="register-form" role="form" method="post" action="{{ route('register') }}" accept-char="UTF-8" autocomplete="off" data-parsley-validate>
                    @csrf
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="input-field">
                            <input type="email" name="email" id="email" value=""
                                    required
                                    data-parsley-trigger="change"
                                    data-parsley-error-message="A valid email address is required.">
                            <label for="email">Email</label>
                            <span class="help-block">Type your email address.</span>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" id="password" value=""
                                    required
                                    data-parsley-trigger="change"
                                    data-parsley-minlength="6"
                                    data-parsley-error-message="The password must be at least 6 characters.">
                            <label for="password">Password</label>
                            <i class="fa fa-eye show-password"></i>
                            <span class="help-block">Type your password.</span>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password_confirmation" id="password_confirmation" value=""
                                    required
                                    data-parsley-trigger="change"
                                    data-parsley-equalto="#password"
                                    data-parsley-error-message="Password does not match.">
                            <label for="password_confirmation">Confirm Password</label>
                            <i class="fa fa-eye show-password"></i>
                            <span class="help-block">Confirm your password.</span>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" name="accept-terms" value="1" id="accept-terms" required/>
                            <label for="accept-terms">I have read and I accept the <a href="#!" target="_blank">Terms Of Use</a></label>
                        </div>

                        <button type="submit" class="btn btn-lg gradient secondary btn-block waves-effect waves-light mt-20 mb-20">Create an account</button>
                        <div class="links">
                            <a href="{{ route('login') }}">Already have an account? Click here</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>

    <script type="text/javascript">

        onAppReady(function() {
            $('.preload').remove();
        });
    </script>

@endsection