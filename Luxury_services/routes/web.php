<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Route::get('/profile', 'ProfileController@show')->name('profile.show');
Route::get('/profile/destroy', 'ProfileController@destroy')->name('profile.delete');
Route::post('/profile', 'ProfileController@store')->name('profile.store');

Auth::routes();

Route::get('/', 'HomeController@index')->name('homepage');
Route::get('/joboffers', 'JobOfferController@index')->name('joboffer');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/about-us', 'HomeController@company')->name('company');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('joboffer/details', 'JobOfferController@show')->name('details');
Route::get('/admin', 'AdminController@show')->name('admin');
Route::get('/jobapplication/{id}', 'JobApplicationController@store');
Route::get('/admin/joboffers', 'AdminController@indexJobOffers')->name('admin.indexJobOffers');
Route::get('/admin/joboffer/{id}', 'AdminController@showJobOffer')->name('admin.showJobOffer');
Route::delete('/admin/joboffer/{id}', 'AdminController@destroyJobOffer')->name('admin.destroyJobOffer');
Route::get('/admin/clients', 'AdminController@showClients')->name('admin.clients');
Route::delete('/admin/clients/{id}', 'AdminController@destroyClient')->name('admin.destroyClient');
//Route::get('/admin/jobapplications{id}', 'AdminController@showJobApplications')->name('admin.jobapplications');

Route::resource('client', 'ClientController');
Route::resource('joboffer', 'JobOfferController');
