<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\JobOffer;
use App\Repositories\Eloquent\EloquentJobOfferRepository;

class AdminController extends Controller
{
  
  public function show()
  {
      $clientList = Client::all();
      return view('admin', compact('clientList'));
  }

  public function indexJobOffers()
  {
    $jobsList = JobOffer::orderBy("created_at", "desc")->get();
    return view('admin.index_job_offers', compact('jobsList', 'totalRounded'));
  }

  public function showJobOffer($id)
  {
    $jobOffer = $this->jobOfferRepository->showOffer($id);
    // get the current job offer
   $jobOfferId = JobOffer::find($id);
   // get previous job offer id
   $previous = JobOffer::where('id', '<', $jobOffer->id)->max('id');
   // get next job offer id
   $next = JobOffer::where('id', '>', $jobOffer->id)->min('id');
    return view('admin.show_job_offer', compact('jobOffer', 'previous', 'next', 'totalRounded'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroyJobOffer($id)
  {
    $item = JobOffer::find($id);
    $item->delete();

    return redirect()->route('admin.index_job_offers');
  }

  public function showClients()
  {
      $clientList = Client::all();
      return view('admin.clients', compact('clientList'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroyClient($id)
  {
    $itemClient = Client::find($id);
    $itemClient->delete();

    return back();
  }
    
}
