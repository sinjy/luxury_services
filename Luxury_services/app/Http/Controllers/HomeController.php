<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobOffer;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobsList = JobOffer::orderBy("created_at", "desc")->take(4)->get();
    
        return view('homepage', compact('jobsList', 'totalRounded'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function company()
    {
        return view('company');
    }
}
