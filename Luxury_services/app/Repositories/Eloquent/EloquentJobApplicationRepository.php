<?php namespace App\Repositories\Eloquent;

use App\JobApplication;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contracts\JobApplicationRepository;
use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentJobApplicationRepository extends AbstractRepository implements JobApplicationRepository
{
    
    public function entity()
    {
        return JobApplication::class;
    }

    public function __construct(JobApplication $model)
   {
        parent::__construct($model);
        $this->model = $model;
   }

   public function hasJobApplication($jobId)
   {
    return $this->model->where('users_id', auth()->id())
                    ->where('job_offers_id', $jobId)
                    ->exists(); 
   }
}
