<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable 
{

    use Notifiable;

    protected $table = 'users';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('gender', 'first_name', 'last_name', 'address', 'country', 'nationality', 'passport', 'curriculum_vitae', 'profile_picture', 'current_location', 'birth_date', 'birth_place', 'email', 'password', 'availability', 'job_sector', 'experience', 'short_description', 'files', 'notes');

    protected $hidden = ['remember_token', 'admin'];

    public function jobOffer()
    {
        return $this->hasMany('App\JobApplication');
    }

}