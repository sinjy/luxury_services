<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model 
{

    protected $table = 'clients';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('society_name', 'society_type', 'contact_name', 'contact_job', 'contact_phone', 'contact_email', 'notes');

    public function jobOffer()
    {
        return $this->hasMany('App\JobOffer');
    }

}